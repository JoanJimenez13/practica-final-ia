class User:
    def __init__(self, name, code):
        self.name = name
        self.code = code


user1 = User('RENE PEREZ JOGLAR', '96-446')
user2 = User('JOAN ADOLFO JIMENEZ CABRAL', '27-881')

# Base de datos de usuarios
user_list = [user1, user2]
