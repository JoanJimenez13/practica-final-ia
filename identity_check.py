import cv2
import os
import pytesseract
from users_database import *

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
face_classif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')

# Parte 1: reconocimiento facial

data_path = './Data'
image_paths = os.listdir(data_path)
print('image_Paths= ', image_paths)

face_recognizer = cv2.face.EigenFaceRecognizer_create()
# Leyendo el modelo
face_recognizer.read('modeloEigenFace.xml')

img = cv2.imread('./TestData/imagen2.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
aux_img = img.copy()
aux_gray = gray.copy()

faces = face_classif.detectMultiScale(gray, 1.3, 5)

for (x, y, w, h) in faces:
	rostro = aux_gray[y:y+h, x:x+w]
	rostro = cv2.resize(rostro, (150, 150), interpolation=cv2.INTER_CUBIC)
	result = face_recognizer.predict(rostro)

	# EigenFaces
	if result[1] < 5700:
			cv2.putText(img, '{}'.format(image_paths[result[0]]), (x, y-25), 2, 1.1, (0,255,0), 2, cv2.LINE_AA)
			cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
	else:
		cv2.putText(img, 'Desconocido', (x, y-20), 2,0.8,(0,0,255),1,cv2.LINE_AA)
		cv2.rectangle(img, (x, y), (x+w, y+h), (0,0,255),2)


# Parte 2: comprobacion de identidad

text = pytesseract.image_to_string(aux_img, lang='spa')
os.system('cls')

for user in user_list:
    if user.name in text:
        if user.code in text:
            print('\n--------------------------------------------------------------\n')
            print('Validado, ' + user.name + ' se encuentra en la base de datos!')
            print('\n--------------------------------------------------------------\n')
        else:
            print('\n--------------------------------------------------------------\n')
            print('No se encontro su codigo de identificacion en la base de datos.')
            print('\n--------------------------------------------------------------\n')
        break
    elif user == user_list[-1]:
        print('\n--------------------------------------------------------------\n')
        print('No se encontro ninguna coincidencia en la base de datos.')
        print('\n--------------------------------------------------------------\n')


cv2.imshow('Comprobacion de identidad', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

file = open('texto.txt', 'w')
file.write(text + '\n')
file.close()
